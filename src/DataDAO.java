import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DataDAO {
	private static final Map<String, UserAccount> mapUsers = new HashMap<String, UserAccount>();
	 
	   static {
	      try {
			initUsers();
		} catch (IOException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   }

	   private static void initUsers() throws FileNotFoundException, IOException, ClassNotFoundException, SQLException {
	 
	      // This user has a role as AGENT.
		   SearchAgent newSearch = new SearchAgent();
			newSearch.getUsername();
			newSearch.getPassword();
			for(int i = 0;i<newSearch.getUsername().size();i++) {
				
				UserAccount name1 = new UserAccount(newSearch.getUsername().get(i),newSearch.getPassword().get(i),UserAccount.GENDER_MALE,SecurityConfig.ROLE_AGENT);
				mapUsers.put(name1.getUserName(), name1);
			}
	      
	 
	      // This user has a role as ADMIN.
	      Properties prop = new Properties();
	     
			prop.load(new FileInputStream("C:\\Users\\Saran\\eclipse-workspace\\LoginApplication\\WebContent\\data.properties"));
			String adminUsername = prop.getProperty("username");
			String adminPassword = prop.getProperty("password");
//			System.out.println(adminPassword);
//			System.out.println(adminUsername);
	      UserAccount admin = new UserAccount(adminUsername, adminPassword, UserAccount.GENDER_MALE, //
	             SecurityConfig.ROLE_ADMIN);
	      mapUsers.put(admin.getUserName(), admin);
	   }
	 
	   // Find a User by userName and password.
	   public static UserAccount findUser(String userName, String password) {
	      UserAccount u = mapUsers.get(userName);
	      if (u != null && u.getPassword().equals(password)) {
	         return u;
	      }
	      return null;
	   }
	
	 
}
