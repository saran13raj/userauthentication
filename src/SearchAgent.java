import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import sun.font.StrikeCache;

public class SearchAgent {
	public List<String> getUsername() throws ClassNotFoundException, SQLException {
		Connection con = DatabaseConection.initializeDatabase();
		Statement st=con.createStatement();
		
		ResultSet rs = st.executeQuery("select username from agentdata");
		List<String> userNameArray=new ArrayList<>();
		String usname = "";
		while(rs.next()) {
			
			userNameArray.add(rs.getString(1));

		}
		return userNameArray;
	}
	public List<String> getPassword() throws ClassNotFoundException, SQLException {
		Connection con = DatabaseConection.initializeDatabase();
		Statement st=con.createStatement();
		
		ResultSet rs = st.executeQuery("select password from agentdata");
		List<String> passwordArray=new ArrayList<>();
		while(rs.next()) {	
			
			passwordArray.add(rs.getString("password"));
			
		}
		return passwordArray;
	}
	
}
