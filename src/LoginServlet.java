

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");

		 dispatcher.forward(request, response);
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 String userName = request.getParameter("userName");
	        String password = request.getParameter("password");
	        DataDAO userData = new DataDAO();
	        UserAccount userAccount = DataDAO.findUser(userName, password);
	 
	        if (userAccount == null) {

	            RequestDispatcher dispatcher //
	                    = this.getServletContext().getRequestDispatcher("/WEB-INF/views/error.jsp");
	 
	            dispatcher.forward(request, response);
	            return;
	        }
	 
	        AppUtils.storeLoginedUser(request.getSession(), userAccount);
	 
	        // 
	        int redirectId = -1;
	        try {
	            redirectId = Integer.parseInt(request.getParameter("redirectId"));
	        } catch (Exception e) {
	        }
	        String requestUri = AppUtils.getRedirectAfterLoginUrl(request.getSession(), redirectId);
	        if (requestUri != null) {
	            response.sendRedirect(requestUri);
	        } else {
	            // Default after successful login
	            // redirect to /userInfo page
	            response.sendRedirect(request.getContextPath() + "/UserInfoServlet");
	        }
	 
		
	}

}
