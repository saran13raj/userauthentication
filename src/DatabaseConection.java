import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConection {

	protected   static Connection initializeDatabase() throws SQLException, ClassNotFoundException {
		String dbDriver = "com.mysql.jdbc.Driver";
		String dbURL = "jdbc:mysql://localhost:3306/";
		
		String dbName = "loginapplication";
		String dbUserName = "root";
		String dbPassword = "password";
		
		Class.forName(dbDriver);
		Connection con = DriverManager.getConnection(dbURL + dbName,dbUserName,dbPassword);
		return con;
		
	}
	

}
